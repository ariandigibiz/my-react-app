import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Banner from '../components/Banner';
import ProductSearch from '../components/ProductSearch';
import FeaturedProducts from '../components/FeaturedProducts';
import Highlights from '../components/Highlights';
import '../App.css'; 

export default function Home() {
  const [searchResult, setSearchResult] = useState(null);
  const productName = "Example Product";
  const location = useLocation();
  const isHomePage = location.pathname === '/';

  const handleSearch = (name) => {
   
  };

  const data = {
    title: "True Skin",
    content: "Confidence in Every Unfiltered Glow",
    destination: "/products",
    label: "Learn More"
  };

  return (
    <>
      <Container>
        <Row>
          <Col>
            <Banner data={data} className="home-banner" />
            <ProductSearch onSearch={handleSearch} productName={productName} productViewUrl="/products" />
          </Col>
        </Row>

        <Row>
          <Col>
            <FeaturedProducts />
          </Col>
        </Row>

        <Row>
          <Col>
            <Highlights />
          </Col>
        </Row>

      </Container>
    </>
  );
}
