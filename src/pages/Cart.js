import React, { useState, useEffect } from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import RemoveButton from '../components/RemoveButton';
import CheckoutButton from '../components/CheckoutButton';

export default function Cart({ userId }) {
  const [cartItems, setCartItems] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchUserCart();
  }, []);

  console.log("BEFORE API CALL");
  
  const fetchUserCart = () => {
    fetch(`https://capstone2-iuz5.onrender.com/users/viewcart`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error(`HTTP error! Status: ${res.status}`);
        }
        return res.json();
      })
      .then((data) => {
        if (data.cartItems !== undefined) {
          setCartItems(data.cartItems);
        } else {
          setError('Cart items are missing or undefined.');
        }
      })
      .catch((error) => {
        console.error('Error fetching user cart:', error);
        setError('An error occurred while fetching user cart: ' + error.message);
      });
  };

  console.log("AFTER FETCH CART API CALL");

  const handleRemove = (removedProductId, newQuantity) => {
    // Update the cartItems state to remove the item with the given productId
    const updatedCartItems = cartItems.filter((item) => item.productId !== removedProductId);
    setCartItems(updatedCartItems);
  };



  
  const handleCheckout = (totalPrice, user) => {
    // Define the logic for handling the checkout here
    console.log('Checkout Logic:', totalPrice, user);
  };

  const setIsLoading = (isLoading) => {
    // Define the logic for setting the loading state here
    console.log('Setting isLoading:', isLoading);
  };

  const clearCart = () => {
    // Define the logic for clearing the cart here
    console.log('Clearing the cart');
  };


  return (
    <>
      {error ? (
        <div className="alert alert-danger" role="alert">
          {error}
        </div>
      ) : (
        <>
          <Row>
            <Col>
              <h1>Cart</h1>
              {cartItems.length > 0 ? (
                cartItems.map((item) => (
                  <Card key={item.productId} className="mb-3">
                    <Card.Body>
                      <Card.Title>Product ID: {item.productId}</Card.Title>
                      <Card.Text>Name: {item.name}</Card.Text>
                      <Card.Text>Price: {item.price}</Card.Text>
                      <Card.Text>Quantity: {item.quantity}</Card.Text>
                      <Card.Text>Total Amount: {item.totalAmount}</Card.Text>
                      
                      {/* Render the RemoveButton component */}
                      <RemoveButton productId={item.productId} onSuccess={handleRemove} />
                      

                      <CheckoutButton
        userId={userId} // Pass the user ID here
        onCheckout={handleCheckout} // Pass the checkout handling function
        setIsLoading={setIsLoading} // Pass the loading state handling function
        clearCart={clearCart} // Pass the cart clearing function
      />

                    </Card.Body>
                  </Card>
                ))
              ) : (
                <div>Your cart is empty.</div>
              )}
            </Col>
          </Row>
        </>
      )}
    </>
  );
};
