import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard'; 
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import '../App.css'; 

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);


  const isAdmin = user && user.isAdmin;

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    fetch(`https://capstone2-iuz5.onrender.com/products/`)
      .then((res) => res.json())
      .then((data) => {
       
        const productsArray = Array.isArray(data) ? data : [data];
        setProducts(productsArray);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };


  const addToCart = (product) => {
    setCart([...cart, product]);
  };

  return (
    <>
      {isAdmin ? (
        <AdminView productsData={products} fetchData={fetchData} />
      ) : (
        <UserView productsData={products} addToCart={addToCart} />
      )}
    </>
  );
}
