import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import ActivateProduct from '../components/ActivateProduct'
import '../App.css'; 

export default function AllProducts() {
  const [allProducts, setAllProducts] = useState([]);

  const fetchData = () => {
 
  fetch(`https://capstone2-iuz5.onrender.com/products/all`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  })
    .then((res) => {
      console.log('Response Status:', res.status); 
      if (!res.ok) {
        throw new Error('Network response was not ok');
      }
      return res.json();
    })
    .then((data) => {
      console.log('API Response Data:', data); 
      if (data && Array.isArray(data)) {
        setAllProducts(data);
      } else {
       
        console.error('API response indicates an error');
      }
    })
    .catch((error) => {
      console.error('Error:', error);
      
    });
};



  useEffect(() => {
    fetchData(); 
  }, []); 

  return (
    <div className="all-products">
      <h1 className="text-center my-4">All Products</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Quantity</th>
          </tr>
        </thead>

        <tbody>
          {allProducts.map((product) => (
            <tr key={product._id}>
              <td>{product._id}</td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.quantity}</td>
              <td className={product.isActive ? 'text-success' : 'text-danger'}>
                {product.isActive ? 'Available' : 'Unavailable'}
              </td>
              <td>
        {/* Use the ArchiveProduct component to handle archiving */}
        <ActivateProduct product={product} isActive={product.isActive} fetchData={fetchData} />
      </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
