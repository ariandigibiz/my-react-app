import React, { useState, useEffect } from 'react';
import { Container, Card, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import AddToCartButton from '../components/AddToCartButton';
import '../App.css'; 

export default function ProductView() {
  const { productId } = useParams();
  const [product, setProduct] = useState({
    name: '',
    description: '',
    price: 0,
  });

  const handleSuccess = () => {
 
  };

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const response = await fetch(`https://capstone2-iuz5.onrender.com/products/${productId}`);
        if (!response.ok) {
          throw new Error('Failed to fetch product data.');
        }
        const data = await response.json();
        setProduct(data);
      } catch (error) {
        console.error('Error while fetching product data:', error);
      }
    };
    fetchProduct();
  }, [productId]);

  return (
    <Container className="product-view">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card.Body className="text-center">
            <Card.Title>{product.name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{product.description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{product.price}</Card.Text>
            <Card.Subtitle></Card.Subtitle>
            <Card.Text></Card.Text>
          </Card.Body>
        </Col>
      </Row>
      <AddToCartButton productId={productId} onSuccess={handleSuccess} />
    </Container>
  );
}
