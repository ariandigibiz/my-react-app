// In your Profile.js component
import { useContext, useState, useEffect } from 'react';
import { Row, Col, Button, Modal } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom'; // Use useNavigate for navigation
import UpdateProfile from '../components/UpdateProfile';
import ResetPassword from '../components/ResetPassword'; // Import the ResetPassword component
import OrderHistory from '../components/OrderHistory';

export default function Profile() {
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate(); // Use useNavigate for navigation
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showResetPasswordModal, setShowResetPasswordModal] = useState(false); // Add state for ResetPassword modal
  const [details, setDetails] = useState({});
  const [profileData, setProfileData] = useState({});

  useEffect(() => {
    fetch(`http://localhost:4000/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('Data from /users/details:', data); // Log the entire data object
        if (data.id !== null) {
          setDetails(data);
          setProfileData(data);
        } else {
          // Redirect to profile if data.id is null
          navigate('/profile');
        }
      });
  }, [navigate]);

  const handleUpdateProfile = ({ newEmail }) => {
    // Update the profileData state with the new data
    setProfileData((prevData) => ({
      ...prevData,
      email: newEmail, // Update the email field
      // You can update other fields here if needed
    }));

    // Update the user's email in the UserContext
    setUser((prevUser) => ({
      ...prevUser,
      email: newEmail, // Update the email field
      // You can update other fields here if needed
    }));
  };

  const handleUpdateButtonClick = () => {
    console.log('Update Profile button clicked');
    setShowUpdateModal(true);
  };

  const handleCloseUpdateModal = () => {
    setShowUpdateModal(false);
  };

  const handleResetPasswordButtonClick = () => {
    console.log('Reset Password button clicked');
    setShowResetPasswordModal(true);
  };

  const handleCloseResetPasswordModal = () => {
    setShowResetPasswordModal(false);
  };

  return (
    <>
      <Row>
        <Col className="p-5 bg-primary text-white">
          <h1 className="my-5">Profile</h1>
          <h2 className="mt-3">{`${profileData.email}`}</h2>
          <button onClick={handleUpdateButtonClick}>Update Profile</button>
          <button onClick={handleResetPasswordButtonClick}>Reset Password</button>
        </Col>
      </Row>
      <Modal show={showUpdateModal} onHide={handleCloseUpdateModal}>
        <Modal.Header closeButton>
          <Modal.Title>Update Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <UpdateProfile userId={user.id} onUpdate={handleCloseUpdateModal} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseUpdateModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showResetPasswordModal} onHide={handleCloseResetPasswordModal}>
        <Modal.Header closeButton>
          <Modal.Title>Reset Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ResetPassword />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseResetPasswordModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <OrderHistory userId={user.id} />
    </>
  );
}
