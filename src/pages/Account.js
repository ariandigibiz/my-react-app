import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Register from '../components/Register'; // Import the Register component
import Login from '../components/Login'; // Import the Login component
import '../App.css';

export default function Account() {
  return (
    <div className="account-container">
      <div className="account-column">
       
        <Register />
      </div>
      <div className="vertical-line"></div>
      <div className="account-column">
        
        <Login />
      </div>
    </div>
  );
}
