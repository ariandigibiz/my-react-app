import React, { useState } from 'react';
import Swal from 'sweetalert2'; // Import SweetAlert2
import 'sweetalert2/dist/sweetalert2.min.css'; // Import SweetAlert2 CSS
import { Button, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom'; // Import Link
import { useParams } from 'react-router-dom';


export default function ProductSearch({ onSearch, productName, productViewUrl }) {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResult, setSearchResult] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const { productId } = useParams();

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    // Use the productName prop in the URL
    fetch(`https://capstone2-iuz5.onrender.com/products/name/${searchQuery}`)
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          setSearchResult(data);
          onSearch(data);
          setShowModal(true); // Show the modal
        } else {
          setSearchResult(null);
          Swal.fire({
            icon: 'warning',
            title: 'Product Not Found',
            text: `Product "${searchQuery}" is not found.`,
          });
        }
      });
  };

  const handleViewProduct = () => {
  if (searchResult) {
    // Construct the correct URL using the productViewUrl prop and the productId from searchResult
    const productUrl = `${productViewUrl}/${searchResult._id}`; // Assuming _id is the correct property for the productId
    
    // Redirect to the product view URL
    window.location.href = productUrl;
  }
};


  return (
    <div>
      <form onSubmit={handleSearchSubmit}>
        <input
          type="text"
          placeholder="Search for a product by name..."
          value={searchQuery}
          onChange={handleSearchChange}
        />
        <button type="submit">Search</button>
      </form>

      {/* Modal */}
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Product Found</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {searchResult ? (
            <>
              {/* Display the product details here */}
              {/* You can add the product details from searchResult */}
              <div>
                <p>Product Name: {searchResult.name}</p>
                <p>Description: {searchResult.description}</p>
                {/* Add a Link to the product view page */}
                <Link to={`${productViewUrl}/${searchResult.productId}`}>
                  <Button variant="primary" onClick={handleViewProduct}>
                    View Product
                  </Button>
                </Link>
              </div>
            </>
          ) : (
            <p>Product not found.</p>
          )}
        </Modal.Body>
      </Modal>
    </div>
  );
}
