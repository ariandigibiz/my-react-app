import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ActivateProduct({ product, isActive, fetchData }) {
  const activateToggle = (productId) => {
   fetch(`https://capstone2-iuz5.onrender.com/products/${productId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.success) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Product successfully activated',
          });
          fetchData();
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error',
            text: 'Please Try again',
          });
          fetchData();
        }
      });
  };

  return (
    <>
      {!isActive && (
        <Button variant="success" size="sm" onClick={() => activateToggle(product._id)}>
          Activate
        </Button>
      )}
    </>
  );
}
