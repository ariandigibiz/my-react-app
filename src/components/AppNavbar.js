import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import '../App.css'; 

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className="custom-navbar">
      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto">
            <Nav.Link as={Link} to="/">
              Home
            </Nav.Link>

           {/* Conditional rendering for the Cart link */}
            {user.id !== null && !user.isAdmin && (
              <Nav.Link as={Link} to="/cart">Cart</Nav.Link>
            )}


            {user.id !== null ? (
              user.isAdmin ? (
                <>
                  <Nav.Link as={Link} to="/products">Products</Nav.Link>
                  <Nav.Link as={Link} to="/users/logout">Logout</Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={Link} to="/products">Products</Nav.Link>
                  <Nav.Link as={Link} to="/users/details">Profile</Nav.Link>    
                  <Nav.Link as={Link} to="/users/logout">Logout</Nav.Link>
                </>
              )
            ) : (
              <>
               <Nav.Link as={Link} to="/account">Account</Nav.Link>
                <Nav.Link as={Link} to="/products">Products</Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
