import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  async function authenticate(e) {
    e.preventDefault();
    try {
      const response = await fetch('https://capstone2-iuz5.onrender.com/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      });

      const data = await response.json();

      if (data.access) {
        localStorage.setItem('token', data.access);

        await retrieveUserDetails(data.access);

        navigate('/products');
      } else {
        throw new Error('Authentication failed');
      }
    } catch (error) {
      console.error('Login error:', error);

      Swal.fire({
        title: 'Authentication failed',
        icon: 'error',
        text: 'Check your login details and try again',
      });
    }

    setEmail('');
    setPassword('');
  }

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          email: data.email, // Include email in the user object
        });

        Swal.fire({
          title: 'Login successful',
          icon: 'success',
          text: 'Welcome to Zuitt!',
        });
      })
      .catch((error) => {
        console.error('Profile retrieval error:', error);
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  if (user && user.id) {
    navigate('/products');
  }

  return (
  <Container className="login-container">
    <Row>
      <Col>
        <Form onSubmit={(e) => authenticate(e)}>
          <h1 className="my-5 text-center">Login</h1>
          <Form.Group controlId="userEmail">
            <Form.Control
              className="custom-input" // Apply the custom-input class
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Control
              className="custom-input" // Apply the custom-input class
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>

          {isActive ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button variant="danger" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Form>
      </Col>
    </Row>
  </Container>
);
}