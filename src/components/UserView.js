import { useState, useEffect } from 'react';
import ProductCard from './ProductCard'; 
import AddToCartButton from './AddToCartButton'; 

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const productsArr = productsData.map((product) => {
      if (product.isActive === true) {
        return (
          <div key={product._id}>
            <ProductCard productProp={product} />
         
          </div>
        );
      } else {
        return null;
      }
    });

    // Set the products state to the result of our map function
    setProducts(productsArr);
  }, [productsData]);

  return (
    <>
      {products.map((product) => (
        <div key={product.key}>{product}</div>
      ))}
    </>
  );
}

