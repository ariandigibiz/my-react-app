import React, { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import ArchiveProduct from './ArchiveProduct';
import EditProduct from './EditProduct';
import AddProduct from './AddProduct';
import { Link } from 'react-router-dom';

export default function AdminView({ productsData, fetchData }) {
  const [showAddProduct, setShowAddProduct] = useState(false);

  const openAddProduct = () => {
    setShowAddProduct(true);
  };

  const closeAddProduct = () => {
    setShowAddProduct(false);
  };

  return (
    <>
      <h1 className="text-center my-4">Admin Dashboard</h1>

      {/* Use the AddProduct component to handle adding new products */}
      <AddProduct fetchData={fetchData} />
      <Link to="/all-products">
        <Button variant="primary" size="sm">
          View All Products
        </Button>
      </Link>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>

        <tbody>
  {productsData.map((product) => (
    <tr key={product._id}>
      <td>{product._id}</td>
      <td>{product.name}</td>
      <td>{product.description}</td>
      <td>{product.price}</td>
        <td>{product.quantity}</td>
      <td className={product.isActive ? "text-success" : "text-danger"}>
        {product.isActive ? "Available" : "Unavailable"}
      </td>
      
      {console.log("Product isActive:", product.isActive)}
      <td>

        <EditProduct product={product} onUpdate={fetchData} />
      </td>
      <td>
     
        <ArchiveProduct product={product} isActive={product.isActive} fetchData={fetchData} />
      </td>
    </tr>
  ))}
</tbody>



      </Table>

      <Modal show={showAddProduct} onHide={closeAddProduct}>
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>Add New Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
           
            <AddProduct
              fetchData={fetchData}
              closeAddProduct={closeAddProduct} 
            />
          </Modal.Body>
        </Form>
      </Modal>
    </>
  );
}
