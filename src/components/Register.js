import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css'; 

export default function Register() {
  const { user } = useContext(UserContext);

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  // Removed unnecessary console.log statements

  function registerUser(e) {
    e.preventDefault();

    fetch(`https://capstone2-iuz5.onrender.com/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          setFirstName('');
          setLastName('');
          setEmail('');
          setPassword('');
          alert('Thank you for registering!');
        } else {
          alert('Please try again');
        }
      });
  }

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      password !== ''
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password]);

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <h1 className="my-5 text-center">Register</h1>

      <Form.Group>
        <Form.Control
          type="text"
          placeholder="First Name"
          required
          value={firstName}
          onChange={(e) => {
            setFirstName(e.target.value);
          }}
          className="custom-input" // Add a custom class for styling
        />
      </Form.Group>

      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Last Name"
          required
          value={lastName}
          onChange={(e) => {
            setLastName(e.target.value);
          }}
          className="custom-input" // Add a custom class for styling
        />
      </Form.Group>

      <Form.Group>
        <Form.Control
          type="email"
          placeholder="Email"
          required
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
          className="custom-input" // Add a custom class for styling
        />
      </Form.Group>

      <Form.Group>
        <Form.Control
          type="password"
          placeholder="Password"
          required
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          className="custom-input" // Add a custom class for styling
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Create
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Create
        </Button>
      )}
    </Form>
  );
}
