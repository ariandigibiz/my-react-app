import React, { useState, useEffect } from 'react';

const OrderHistory = () => {
  const [orderHistory, setOrderHistory] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Fetch order history data
    fetchOrderHistory();

    async function fetchOrderHistory() {
      try {
        const response = await fetch(`http://localhost:4000/users/order-history`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        });

        if (response.ok) {
          const data = await response.json();
          setOrderHistory(data.orderHistory);
        } else {
          console.error('Failed to fetch order history.');
        }
      } catch (error) {
        console.error('Error fetching order history:', error);
      } finally {
        setLoading(false);
      }
    }
  }, []);

  return (
    <div>
      <h3>Order History</h3>
      {loading ? (
        <p>Loading order history...</p>
      ) : (
        <ul>
          {orderHistory.map((order, index) => (
            <li key={index}>
              {/* Display order details here */}
              <div>
                <strong>Order Date:</strong> {new Date(order.purchasedOn).toLocaleDateString()}
              </div>
              <div>
                <strong>Total Amount:</strong> ${order.totalAmount.toFixed(2)}
              </div>
              <div>
                <strong>Ordered Products:</strong>
                <ul>
                  <li>
                    {order.products.name} (Quantity: {order.products.quantity})
                  </li>
                </ul>
              </div>
              {/* Add more order details as needed */}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default OrderHistory;
