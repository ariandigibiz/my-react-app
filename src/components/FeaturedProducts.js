import { useState, useEffect } from 'react';
import { Card, Container, Row, Col } from 'react-bootstrap';
import PreviewProducts from './PreviewProducts';

export default function FeaturedProducts({productId}) {
  const [previews, setPreviews] = useState([]);

  useEffect(() => {
    fetch(`https://capstone2-iuz5.onrender.com/products/`)
      .then((res) => res.json())
      .then((data) => {
        const productLength = Math.min(data.length, 3); 

        const randomProducts = [...data]; 

        
        for (let i = randomProducts.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [randomProducts[i], randomProducts[j]] = [randomProducts[j], randomProducts[i]];
        }

        const featuredProducts = randomProducts.slice(0, productLength).map((product) => (
          <Col key={product._id}>
            <PreviewProducts data={product} productId={product._id}/>
          </Col>
        ));

        setPreviews(featuredProducts);
      });
  }, []);

  return (
    <>
      <h2 className="text-center">Best-Selling</h2>
      <Row>{previews}</Row>
    </>
  );
}
