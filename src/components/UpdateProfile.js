import React, { useState } from 'react';
import Swal from 'sweetalert2';

const UpdateProfile = ({ userId, onUpdate }) => {
  const [newEmail, setNewEmail] = useState('');
  
  const [isLoading, setIsLoading] = useState(false);

  const handleUpdateProfile = async () => {
    setIsLoading(true);

    try {
      const response = await fetch(`https://capstone2-iuz5.onrender.com/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({ newEmail }), // Send only the new email to the server
      });

      if (response.ok) {
        // Profile updated successfully
        Swal.fire({
          title: 'Profile Updated',
          text: 'Your profile has been updated successfully.',
          icon: 'success',
          confirmButtonText: 'Close',
        });

        // Call the onUpdate callback to notify the parent component
        onUpdate();
      } else {
        // Profile update failed
        const errorMessage = await response.text();
        Swal.fire({
          title: 'Profile Update Failed',
          text: `An error occurred: ${errorMessage}`,
          icon: 'error',
          confirmButtonText: 'Close',
        });
      }
    } catch (error) {
      console.error('Error updating profile:', error);
      Swal.fire({
        title: 'Profile Update Failed',
        text: 'An error occurred while updating your profile. Please try again later.',
        icon: 'error',
        confirmButtonText: 'Close',
      });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div>
      <h3>Update Profile</h3>
      <div>
        <label>New Email:</label>
        <input type="email" value={newEmail} onChange={(e) => setNewEmail(e.target.value)} />
      </div>
      
      <button onClick={handleUpdateProfile} disabled={isLoading}>
        {isLoading ? 'Updating...' : 'Update'}
      </button>
    </div>
  );
};

export default UpdateProfile;
