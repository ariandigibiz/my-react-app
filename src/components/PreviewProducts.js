import React from 'react';
import { Card, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'; 

export default function PreviewProducts({ data, productId }) {
  return (
    <Row>
      <Col xs={12} md={12}>
        <div>
          <Card style={{ height: '70%' }} className="cardPreviewProducts">
            <Card.Img variant="top" src={data.imageUrl} />
            <Card.Body>
              <Card.Title>{data.name}</Card.Title>
              <Card.Text style={{ whiteSpace: 'normal' }}>
                {data.highlight}
              </Card.Text>
              <Link to={`/products/${productId}`} className="btn btn-success">
                Read More
              </Link>
            </Card.Body>
          </Card>
        </div>
      </Col>
    </Row>
  );
}
