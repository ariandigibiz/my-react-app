import React, { useState } from 'react';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

const CheckoutButton = ({ userId, onCheckout, setIsLoading, clearCart }) => {
  const [isLoading, setIsLoadingLocal] = useState(false);
  const navigate = useNavigate();

  const handleCheckout = async () => {
    try {
      setIsLoadingLocal(true);

      const checkoutUrl = `https://capstone2-iuz5.onrender.com/users/checkout`;



      // Perform the checkout request
      const response = await fetch(checkoutUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({ userId }),
      });

      if (response.ok) {
        const data = await response.json();

        // Pass the onCheckout function as a prop from your parent component
        onCheckout(data.totalPrice, data.user);

        // Display a success message using Swal
        Swal.fire({
          title: 'Checkout Success',
          text: 'Your order has been placed successfully!',
          icon: 'success',
          confirmButtonText: 'Close',
          onClose: () => {
            // Clear the cart after the alert is closed
            clearCart();
          },
        });

        // Redirect to the home page or perform other actions
        navigate('/'); // Use the navigate function
      } else {
        const errorMessage = await response.text();
        console.error('Checkout failed:', errorMessage);

        // Display an error message using Swal
        Swal.fire({
          title: 'Checkout Failed',
          text: 'An error occurred during checkout. Please try again later.',
          icon: 'error',
          confirmButtonText: 'Close',
        });
      }
    } catch (error) {
      console.error('Error during checkout:', error);

      // Display an error message using Swal
      Swal.fire({
        title: 'Checkout Failed',
        text: 'An error occurred during checkout. Please try again later.',
        icon: 'error',
        confirmButtonText: 'Close',
      });
    } finally {
      setIsLoadingLocal(false);
      // Pass the setIsLoading function as a prop from your parent component
      setIsLoading(false);
    }
  };

  return (
    <button onClick={handleCheckout} disabled={isLoading}>
      {isLoading ? 'Processing...' : 'Checkout'}
    </button>
  );
};

export default CheckoutButton;
