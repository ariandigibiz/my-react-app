import React from 'react';
import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap';

const RemoveButton = ({ productId, onSuccess }) => {
  const handleRemoveFromCart = () => {
    fetch(`https://capstone2-iuz5.onrender.com/users/cart/remove/${productId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (response.ok) {
          Swal.fire({
            title: 'Removed From Cart Successful',
            icon: 'success',
            text: 'Product removed from cart successfully!',
          });

          // Notify the parent component with the updated quantity (0)
          onSuccess(productId, 0);
        } else {
          console.error('Failed to remove the product from the cart.');

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Failed to remove the product from the cart.',
          });
        }
      })
      .catch((error) => {
        console.error('An error occurred while removing the product from the cart:', error);
      });
  };

  return (
    <div>
      <Button variant="danger" onClick={handleRemoveFromCart}>
        Remove
      </Button>
    </div>
  );
};

export default RemoveButton;
