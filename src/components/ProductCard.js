import React from 'react';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import AddToCartButton from './AddToCartButton';

export default function ProductCard({ productProp }) {
  const { _id, name, description, price } = productProp;


  const handleSuccess = () => {
   
    console.log('Product added to cart successfully.');
    
  };

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        {/* Use the AddToCartButton component to handle adding the product to the cart */}
        <AddToCartButton productId={_id} onSuccess={handleSuccess} />
      </Card.Body>
    </Card>
  );
}


ProductCard.propTypes = {
  productProp: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
