import React, { useState } from 'react';
import Swal from 'sweetalert2'; // Import SweetAlert
import { Button, Form } from 'react-bootstrap';

const AddToCartButton = ({ productId, initialQuantity, onSuccess }) => {
  const [quantity, setQuantity] = useState(initialQuantity || 0);

  const handleIncrement = () => {
    setQuantity(quantity + 1);
  };

  const handleDecrement = () => {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  };

  const handleAddToCart = () => {
    fetch(`https://capstone2-iuz5.onrender.com/users/cart/add/${productId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId,
        quantity, // Set the quantity to the current value
      }),
    })
      .then((response) => {
        if (response.ok) {
          Swal.fire({
            title: 'Added To Cart Successful',
            icon: 'success',
            text: 'Product added to cart successfully!',
          });

          onSuccess(quantity); // Notify the parent component with the updated quantity
          console.log(quantity);
        } else {
          console.error('Failed to add the product to the cart.');

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Failed to add the product to the cart.',
          });
        }
      })
      .catch((error) => {
        console.error('An error occurred while adding the product to the cart:', error);
      });
  };

  return (
    <div>
      <Form.Group className="mb-3">
        <Form.Label>Quantity to Buy</Form.Label>
        <div className="d-flex align-items-center">
          <Button className="m-1" variant="info" onClick={handleDecrement}>
            -
          </Button>
          <Form.Control
            type="number"
            value={quantity}
            onChange={(e) => setQuantity(Number(e.target.value))}
            min="0"
          />
          <Button className="m-1" variant="info" onClick={handleIncrement}>
            +
          </Button>
        </div>
      </Form.Group>

      <Button onClick={handleAddToCart}>Add to Cart</Button>
    </div>
  );
};

export default AddToCartButton;

