import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddProduct({ fetchData, productData }) {
  const [loading, setLoading] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [newProduct, setNewProduct] = useState({
    name: '',
    description: '',
    highlight:'',
    price: '',
    
  });

  const handleShowAddModal = () => setShowAddModal(true);
  const handleCloseAddModal = () => {
    setShowAddModal(false);
    
    setNewProduct({
      name: '',
      description: '',
      highlight:'',
      price: '',
     
    });
  };

  const handleAddProduct = () => {
    setLoading(true);

    console.log("Before API");

    fetch(`https://capstone2-iuz5.onrender.com/products`, {
  method: 'POST', 
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${localStorage.getItem('token')}`,
  },
  body: JSON.stringify(newProduct), 
})
      .then((res) => res.json())
      .then((data) => {
  console.log(newProduct);
  if (data && data.success) {
    handleCloseAddModal();
    fetchData();
    setLoading(false);

    Swal.fire({
      title: 'Product Added Successfully',
      icon: 'success',
      text: 'You have successfully added a new product.',
    });
  } else {
    setLoading(false);

    Swal.fire({
      title: 'Something went wrong',
      icon: 'error',
      text: 'Please try again.',
    });
  }
})
      .catch((error) => {
        console.error('Error while adding the product:', error);
        setLoading(false);
      });
  };

  return (
    <>
      <Button variant="success" onClick={handleShowAddModal}>
        Add Product
      </Button>

      <Modal show={showAddModal} onHide={handleCloseAddModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={newProduct.name}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, name: e.target.value })
                }
              />
            </Form.Group>

            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product description"
                value={newProduct.description}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, description: e.target.value })
                }
              />
            </Form.Group>

             <Form.Group controlId="productHighlight">
              <Form.Label>Highlight</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product highlight"
                value={newProduct.highlight}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, highlight: e.target.value })
                }
              />
            </Form.Group>


            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter product price"
                value={newProduct.price}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, price: e.target.value })
                }
              />
            </Form.Group>

            
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseAddModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleAddProduct} disabled={loading}>
            {loading ? 'Adding...' : 'Add Product'}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
