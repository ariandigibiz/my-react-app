import Container from 'react-bootstrap/Container';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar';


import Home from './pages/Home';
import Products from './pages/Products';
import Profile from './pages/Profile';
import Logout from './pages/Logout';
import ProductView from './pages/ProductView';
import Error from './pages/Error';

import Cart from './pages/Cart';
import AllProducts from './pages/AllProducts';
import Account from './pages/Account';

import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const storedToken = localStorage.getItem('token');


  const [user, setUser] = useState({
    token: storedToken, 
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.removeItem('token'); 
    setUser({
      token: null, 
      id: null,
      isAdmin: null,
    });
  };

  useEffect(() => {
    if (user.token) {
   
      fetch(`https://capstone2-iuz5.onrender.com/users/details`, {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          console.log("App.js");
          if (typeof data._id !== "undefined") {
            setUser({
              token: user.token,
              id: data._id,
              isAdmin: data.isAdmin,
            });
          } else {
            setUser({
              token: null, 
              id: null,
              isAdmin: null,
            });
          }
        })
        .catch((error) => {
          console.error('Profile retrieval error:', error);
          setUser({
            token: null, 
            id: null,
            isAdmin: null,
          });
        });
    }
  }, [user.token]);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
    <Router>
      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />
      
          <Route path="/products" element={<Products />} />
          <Route exact path="/products/:productId" element={<ProductView />} />
          
          <Route path="/users/logout" element={<Logout />} />
          <Route path="/users/details" element={<Profile />} />
       
         <Route path="/cart" element={<Cart />} />
          <Route path="*" element={<Error />} />
          <Route path="/all-products" element={<AllProducts />} />

          {/* Route for the Account component (which contains Register and Login) */}
            <Route path="/account/*" element={<Account />} />

        </Routes>
      </Container>
    </Router>
  </UserProvider>


  );
}

export default App;
