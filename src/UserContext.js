import React from 'react';

const UserContext = React.createContext();

export const UserProvider = ({ children, value }) => (
  <UserContext.Provider value={value}>{children}</UserContext.Provider>
);

export default UserContext;
